'''
Target: Python AndroidToolBox Basic Customized System Methods Module
Author: LZR@BUAA
Date:   08/26/2020
'''
import io
import os
import re
import sys
import tarfile

from ADBMethods import *
from main import mainmenu


def cmd(command):
    r = os.popen(command).read()
    return r


def unziptar(filename, dirs):
    t = tarfile.open(filename)
    t.extractall(path=dirs)
    t.close()


def InputJudge(num):
    if num == 2:
        while True:
            command = input()
            if command not in ['Y', 'y', 'N', 'n']:
                # if(input != "Y" and input != "N"and input != "y"and input != "n"):
                print("输入有误，请检查输入！")
            else:
                if command in ['y', 'Y']:
                    return True
                else:
                    return False
    else:
        while True:
            command = input()
            if command.isdigit():
                intput = int(command)
                if intput not in range(1, num+1):
                    print("输入有误，请检查输入！")
                else:
                    return intput
            else:
                print("输入有误，请检查输入！")


def NewProgram(pyFileName):
    os.system('cls')
    # print("Press Enter to Continue……")
    print(cmd("python "+pyFileName))


def dragtoWindowGetName():
    filedir = input("请将文件拖动到窗口，并点击窗口后按下Enter")
    return filedir


def exitProgram(num):
    if num == 0:
        print("操作已成功完成，程序退出中……")
    elif num == 1:
        print("用户终止操作，程序退出中……")
    elif num == 3:
        print("出现异常，程序退出中……")
