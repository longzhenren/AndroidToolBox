'''
Target: Python AndroidToolBox ADB Plugin Methods Module
Author: LZR@BUAA
Date:   08/26/2020
'''
import os
import io
import sys
import re
from SysMethods import *


IP = ""
MobileInfo = {}

'''
Check Connections:
'''


def getFastbootConnection():
    cmd("fastboot devices")


def USBConnected():
    echo = cmd("adb devices").split("\n")
    return echo[1] != ''


def BLConnected():
    echo = cmd("fastboot devices").find("fastboot")
    return echo != -1


def WLANConnected():
    if powerStatus():
        IP = getIPaddress()
        r = cmd("adb devices")
        if r.find(IP) == -1:
            return False
        else:
            return True


'''
Reboot:
'''


def rebootUI():
    if BLConnected():
        cmd("fastboot reboot")
    if(USBConnected):
        cmd("adb reboot")


def rebootBL():
    cmd("adb reboot fastboot")


def rebootRec():
    cmd("adb reboot bootloader")


def fastbootReboot():
    cmd("fastboot reboot")


'''
Get Info:
'''


def getInfo():
    model = cmd("adb shell getprop ro.product.model").replace(
        '\n', '').replace('\r', '')
    manufacturer = cmd("adb shell getprop ro.product.brand").replace(
        '\n', '').replace('\r', '')
    lockstatus = cmd("adb shell getprop ro.secureboot.devicelock").replace(
        '\n', '').replace('\r', '')
    APIver = cmd("adb shell getprop ro.build.version.sdk").replace(
        '\n', '').replace('\r', '')
    wlandevice = cmd("adb shell getprop wifi.interface").replace(
        '\n', '').replace('\r', '')
    Serial = cmd("adb shell getprop ro.boot.serialno").replace(
        '\n', '').replace('\r', '')
    version = cmd("adb shell getprop ro.build.version.release").replace(
        '\n', '').replace('\r', '')

    MobileInfo.update({'model': model})
    MobileInfo.update({'API': APIver})
    MobileInfo.update({'brand': manufacturer})
    MobileInfo.update({'Wlan': wlandevice})
    MobileInfo.update({'serial': Serial})
    MobileInfo.update({'lock': lockstatus})
    MobileInfo.update({'android': version})

    return MobileInfo


def lockStatus():
    rebootBL()
    while True:
        if BLConnected():
            break
    r = cmd("fastboot oem device-info | findstr unlocked")
    if r.split(":")[-1].replace(" ", "") == "true":
        return True
    else:
        return False


def getBattery():
    r = cmd("adb shell dumpsys battery | findstr \"level\"").split(": ")[-1].replace(
        '\n', '').replace('\r', '')
    MobileInfo.update({'battery': r})
    return r


def getIPaddress():
    netstatus = cmd("adb shell ip addr | findstr \"wlan0\"")
    # print(netstatus)
    IP = re.findall(r"\b(?:[0-9]{1,3}\.){3}[0-9]{1,3}\b", netstatus)
    return IP[0]


def powerStatus():
    PowerStat = cmd("adb shell getprop sys.boot_completed")
    if PowerStat == "\n":
        return False
    else:
        return True


'''
File Operations:
'''


def pushFile(sourcedir, rmpath):
    print(cmd("adb push " + sourcedir + " " + rmpath))


def pullFile(rmpath, destdir):
    print(cmd("adb pull " + rmpath + " " + destdir))


def dragtoPush(rmpath):
    dir = dragtoWindowGetName()
    pushFile(dir, rmpath)


def touchPoint(x, y):
    cmd("adb shell input tap "+x+" "+y)


def slidePoint(x1, y1, x2, y2):
    cmd("adb shell input swipe "+x1+" "+y1+" "+x2+" "+y2)


# if __name__ == "__main__":
#     getBattery()
